//
//  ModuleThreeRouter.swift
//  ModuleThree
//
//  Created by Гутовец Дмитрий on 29.04.2021.
//

import Foundation
import UIKit

import ModuleDeeplinks


@objc public class ModuleThreeRouter: NSObject, DeeplinksRouter {
    
    public struct DeeplinkNames {
        public static let nameThreeOne = "namethreeone"
        public static let nameThreeTwo = "namethreetwo"
    }
    
    @objc public override init() {
        print("- \(type(of: self)) inited")
        super.init()
    }
    
    
    // MARK: - Private
    private func navigate(view: UIViewController, with deeplink: Deeplink, navType: DeeplinkNavigationType) {
        switch navType {
        case .push:
            deeplink.sourceView?.navigationController?.pushViewController(view, animated: true)
        case .present:
            deeplink.sourceView?.present(view, animated: true, completion: nil)
        case .presentAfterDismiss:
            guard let sourceController = deeplink.sourceView,
                  let sourcePresentingController = sourceController.presentingViewController else {
                return
            }
            sourceController.dismiss(animated: true) { [sourcePresentingController, view] in
                sourcePresentingController.present(view, animated: true, completion: nil)
            }
        }
    }
    
    
    // MARK: - DeeplinksRouter
    
    public func canOpen(_ deeplink: Deeplink) -> Bool {
        var result = false
        let url = deeplink.sourceData
        let host = url.host
        if host == "three" {
            let firstPathComp = url.pathComponents[1]
            result = [DeeplinkNames.nameThreeOne,
                      DeeplinkNames.nameThreeTwo].contains(firstPathComp)
        }
        return result
    }
    
    public func open(_ deeplink: Deeplink) {
        if canOpen(deeplink) {
            // Что-то делаем / Открываем
            let name = deeplink.sourceData.pathComponents[1]
            if name == DeeplinkNames.nameThreeOne {
                let view = ThreeOneViewController()
                navigate(view: view, with: deeplink, navType: .push)
            } else if name == DeeplinkNames.nameThreeTwo {
                let view = ThreeTwoViewController()
                navigate(view: view, with: deeplink, navType: .present)
            }
        }
    }
}
