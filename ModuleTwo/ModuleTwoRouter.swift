//
//  ModuleTwoRouter.swift
//  ModuleTwo
//
//  Created by Гутовец Дмитрий on 29.04.2021.
//

import Foundation
import UIKit

import ModuleDeeplinks


@objc public class ModuleTwoRouter: NSObject, DeeplinksRouter {
    
    public struct DeeplinkNames {
        public static let nameTwoOne = "pathtwoone"
    }
    
    @objc public override init() {
        print("- \(type(of: self)) inited")
        super.init()
    }
    
    
    // MARK: - Private
    private func navigate(view: UIViewController, with deeplink: Deeplink) {
        deeplink.sourceView?.navigationController?.pushViewController(view, animated: true)
    }
    
    
    // MARK: - DeeplinksRouter
    
    public func canOpen(_ deeplink: Deeplink) -> Bool {
        var result = false
        let url = deeplink.sourceData
        let host = url.host
        if host == "two" {
            let firstPathComp = url.pathComponents[1]
            result = [DeeplinkNames.nameTwoOne].contains(firstPathComp)
        }
        return result
    }
    
    public func open(_ deeplink: Deeplink) {
        if canOpen(deeplink) {
            // Что-то делаем / Открываем
            let name = deeplink.sourceData.pathComponents[1]
            if name == DeeplinkNames.nameTwoOne {
                let view = TwoOneViewController(source: deeplink.sourceType)
                navigate(view: view, with: deeplink)
            }
        }
    }
}
