//
//  TwoOneViewController.swift
//  ModuleTwo
//
//  Created by Гутовец Дмитрий on 29.04.2021.
//

import UIKit

import ModuleDeeplinks

class TwoOneViewController: UIViewController {
    
    let openedSource: DeeplinkSourceType

    public init(source: DeeplinkSourceType) {
        self.openedSource = source
        super.init(nibName: String(describing: type(of: self)), bundle: Bundle(for: type(of: self).self))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if openedSource == .deeplink {
            let alertView = UIAlertController(title: "Внимание", message: "Определили тип вызвавшего события и можем, например, отправить в аналитику", preferredStyle: .alert)
            alertView.addAction(UIAlertAction(title: "Close", style: .cancel, handler: nil))
            present(alertView, animated: true, completion: nil)
        }
    }

}
