//
//  ViewController.swift
//  Deeplinks
//
//  Created by Дмитрий Гутовец on 29.04.2021.
//

import UIKit

import ModuleDeeplinks

import ModuleOne

@objc class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    private var dataSource: [DeeplinkDataItem] = DeeplinkDataItem.allValues()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.separatorStyle = .none
        tableView.registerCell(for: DeeplinkItemTableViewCell.self)
        tableView.dataSource = self
        tableView.delegate = self
        
//        navigationController?.pushViewController(OneOneViewController(), animated: true)
    }

    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = dataSource[indexPath.item]
        var cell: UITableViewCell!
        if let itemCell = tableView.dequeueReusableCell(withIdentifier: type(of: model).reuseIdentifier, for: indexPath) as? DeeplinkItemTableViewCell {
            itemCell.config(with: model)
            cell = itemCell
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let model = dataSource[indexPath.item]
        
        let urlScheme = "deeplinks://"
        let deeplink = model.fill(deeplink: urlScheme)
        
        if UIApplication.shared.canOpenURL(deeplink) {
            UIApplication.shared.open(deeplink, options: [:], completionHandler: nil)
        }
    }
}
