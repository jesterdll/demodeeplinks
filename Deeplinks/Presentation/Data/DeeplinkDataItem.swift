//
//  DeeplinkDataItem.swift
//  Deeplinks
//
//  Created by Гутовец Дмитрий on 29.04.2021.
//

import Foundation

import ModuleOne
import ModuleTwo
import ModuleThree
import ModuleFour

// Технический класс - для отрисовки таблицы
class DeeplinkDataItem {
    static let reuseIdentifier: String = DeeplinkItemTableViewCell.reuseIdentifier
    
    var title: String
    var value: String
    var host: String
    var detail: String?
    var params: Any?
    
    init(title: String, host: String, value: String, detail: String? = nil, params: Any? = nil) {
        self.title = title
        self.host = host
        self.value = value
        self.detail = detail
        self.params = params
    }
    
    func fill(deeplink url: String) -> URL {
        var resultURL: String = url
        resultURL += host + "/" + value + "/"
        
        let deeplinkURL = URL(string: resultURL)!
        return deeplinkURL
    }
}

// Для создание тестовых данных
extension DeeplinkDataItem {
    static let deeplink_1_1: DeeplinkDataItem = DeeplinkDataItem(title: "Push к экрану 1.1",
                                                                 host: "one",
                                                                 value: ModuleOneRouter.DeeplinkNames.nameOneOne)
    static let deeplink_1_2: DeeplinkDataItem = DeeplinkDataItem(title: "Present экрана 1.2",
                                                                 host: "one",
                                                                 value: ModuleOneRouter.DeeplinkNames.nameOneTwo)
    static let deeplink_1_3: DeeplinkDataItem = DeeplinkDataItem(title: "Present After Dismiss (запуск через 1.2)",
                                                                 host: "one",
                                                                 value: ModuleOneRouter.DeeplinkNames.nameOneThree)
    
    static let deeplink_2_1: DeeplinkDataItem = DeeplinkDataItem(title: "Push к экрану 2.1",
                                                                 host: "two",
                                                                 value: ModuleTwoRouter.DeeplinkNames.nameTwoOne,
                                                                 detail: "Учитываем source")
    
    static let deeplink_3_1: DeeplinkDataItem = DeeplinkDataItem(title: "Push к экрану 3.1",
                                                                 host: "three",
                                                                 value: ModuleThreeRouter.DeeplinkNames.nameThreeOne)
    static let deeplink_3_2: DeeplinkDataItem = DeeplinkDataItem(title: "Present экрана 3.2",
                                                                 host: "three",
                                                                 value: ModuleThreeRouter.DeeplinkNames.nameThreeTwo)
    
    static let deeplink_4_1: DeeplinkDataItem = DeeplinkDataItem(title: "Push к экрану 4.1 (ObjC)",
                                                                 host: "four",
                                                                 value: FourRouterDeeplinkNames.nameFourOne())
    
    static func allValues() -> [DeeplinkDataItem] {
        return [DeeplinkDataItem.deeplink_1_1, DeeplinkDataItem.deeplink_1_2,
                DeeplinkDataItem.deeplink_1_3, DeeplinkDataItem.deeplink_2_1,
                DeeplinkDataItem.deeplink_3_1, DeeplinkDataItem.deeplink_3_2,
                DeeplinkDataItem.deeplink_4_1]
    }
}
