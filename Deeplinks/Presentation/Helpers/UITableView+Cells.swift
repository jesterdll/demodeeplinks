//
//  UITableView+Cells.swift
//  Deeplinks
//
//  Created by Гутовец Дмитрий on 29.04.2021.
//

import UIKit

protocol Reusable {
    static var reuseIdentifier: String { get }
}

extension UITableViewCell: Reusable {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

extension UITableView {
    func registerCell(for classCell: UITableViewCell.Type) {
        let reuseIdentifier = classCell.reuseIdentifier
        let bundle = Bundle(for: classCell)
        let nib = UINib(nibName: reuseIdentifier, bundle: bundle)
        self.register(nib, forCellReuseIdentifier: reuseIdentifier)
    }
}
