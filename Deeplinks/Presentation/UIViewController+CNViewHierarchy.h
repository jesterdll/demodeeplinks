//
//  UIViewController+CNViewHierarchy.h
//  Deeplinks
//
//  Created by Гутовец Дмитрий on 30.04.2021.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol CNContainerViewControllerProtocol <NSObject>

@property (strong,nonatomic, readonly) UIViewController *childViewController;

@end


@interface UIViewController (CNViewHierarchy)

+ (UIViewController *)cn_topViewController;
+ (UIViewController *)cn_topViewController:(BOOL)excludingAlertViewController;
+ (BOOL)cn_isTopViewControllerInTransition;

- (BOOL)cn_isViewControllerInTransition;

@end

NS_ASSUME_NONNULL_END
