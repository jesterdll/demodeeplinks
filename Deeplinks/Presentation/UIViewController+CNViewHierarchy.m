//
//  UIViewController+CNViewHierarchy.m
//  Deeplinks
//
//  Created by Гутовец Дмитрий on 30.04.2021.
//

#import "UIViewController+CNViewHierarchy.h"

@implementation UIViewController (CNViewHierarchy)

+ (UIViewController *)cn_topViewController {
    UIViewController *topViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (YES) {
        if (topViewController.presentedViewController) {
            topViewController = topViewController.presentedViewController;
        } else if ([topViewController conformsToProtocol:@protocol(CNContainerViewControllerProtocol)]) {
            id<CNContainerViewControllerProtocol> con = (id<CNContainerViewControllerProtocol>)topViewController;
            if (con.childViewController != nil) {
                topViewController = con.childViewController;
            } else {
                break;
            }
        } else if ([topViewController isKindOfClass:[UITabBarController class]]) {
            UITabBarController *tab = (UITabBarController *)topViewController;
            topViewController = tab.selectedViewController;
        } else if ([topViewController isKindOfClass:[UINavigationController class]]) {
            UINavigationController *nav = (UINavigationController *)topViewController;
            topViewController = nav.topViewController;
        } else {
            break;
        }
    }
    
    return topViewController;
}

+ (UIViewController *)cn_topViewController:(BOOL)excludingAlertViewController {
    UIViewController *topViewController = [self cn_topViewController];
    
    while (excludingAlertViewController && [topViewController isKindOfClass:[UIAlertController class]]) {
        topViewController = topViewController.presentingViewController;
    }
    
    return topViewController;
}

+ (BOOL)cn_isTopViewControllerInTransition {
    UIViewController *topViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (YES) {
        
        if ([topViewController cn_isViewControllerInTransition]) {
            return YES;
        }
        
        if (topViewController.presentedViewController) {
            topViewController = topViewController.presentedViewController;
        } else if ([topViewController conformsToProtocol:@protocol(CNContainerViewControllerProtocol)]) {
            id<CNContainerViewControllerProtocol> con = (id<CNContainerViewControllerProtocol>)topViewController;
            if (con.childViewController != nil) {
                topViewController = con.childViewController;
            } else {
                break;
            }
        } else if ([topViewController isKindOfClass:[UITabBarController class]]) {
            UITabBarController *tab = (UITabBarController *)topViewController;
            topViewController = tab.selectedViewController;
        } else if ([topViewController isKindOfClass:[UINavigationController class]]) {
            UINavigationController *nav = (UINavigationController *)topViewController;
            topViewController = nav.topViewController;
        } else {
            break;
        }
    }
    
    return NO;
}

- (BOOL)cn_isViewControllerInTransition {
    if (self.isMovingFromParentViewController || self.isMovingToParentViewController ||
        self.isBeingDismissed || self.isBeingPresented) {
        return YES;
    }
    return NO;
}

@end
