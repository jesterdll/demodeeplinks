//
//  DeeplinkItemTableViewCell.swift
//  Deeplinks
//
//  Created by Гутовец Дмитрий on 29.04.2021.
//

import UIKit

final class DeeplinkItemTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDetail: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        backgroundColor = UIColor.white
    }
    
    func config(with model: DeeplinkDataItem) {
        labelTitle.text = model.title
        let detailText = model.detail ?? ""
        labelDetail.text = detailText
    }
}
