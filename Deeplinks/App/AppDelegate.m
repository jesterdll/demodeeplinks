//
//  AppDelegate.m
//  Deeplinks
//
//  Created by Гутовец Дмитрий on 08.05.2021.
//

#import "AppDelegate.h"
#import "UIViewController+CNViewHierarchy.h"
#import "Deeplinks-Swift.h"

// Модуль диплинков
#import <ModuleDeeplinks/ModuleDeeplinks.h>
// Сторонние модули
#import <ModuleOne/ModuleOne.h>
#import <ModuleTwo/ModuleTwo.h>
#import <ModuleThree/ModuleThree.h>
#import <ModuleFour/ModuleFour.h>

@interface AppDelegate () <DeeplinksManagerDelegate>

//@property (strong, nonatomic) DeeplinksManager *deeplinksManager;
@property (strong, nonatomic) id<DeeplinksManagerActionable, DeeplinksManagerRegisterable> deeplinksManager;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    self.deeplinksManager = [[DeeplinksMainManager alloc] initWithDelegate:self];
    [self getListRouters];
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *startNav = [sb instantiateViewControllerWithIdentifier:@"startNavigation"];
    
    [self.window setRootViewController:startNav];
    [self.window makeKeyAndVisible];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
}


#pragma mark - Deeplinks handler

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options {
    [self.deeplinksManager openWithRaw:url source:DeeplinkSourceTypeDeeplink];
    return YES;
}


#pragma mark - Routers

- (void)getListRouters {
    NSArray *routers = @[[[ModuleOneRouter alloc] init],
                         [[ModuleTwoRouter alloc] init],
                         [[ModuleThreeRouter alloc] init],
                         [[ModuleFourRouter alloc] init]];
    [self.deeplinksManager registerWithRouters:routers];
}


#pragma mark - DeeplinksManagerDelegate

- (UIViewController *)getSourceView {
    return [UIViewController cn_topViewController];
}

@end
