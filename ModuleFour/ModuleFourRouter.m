//
//  ModuleFourRouter.m
//  ModuleFour
//
//  Created by Гутовец Дмитрий on 05.05.2021.
//

#import "ModuleFourRouter.h"

#import "FourRouterDeeplinkNames.h"
#import "FourOneViewController.h"


@interface ModuleFourRouter()

@property (strong, nonatomic) FourRouterDeeplinkNames *deeplinkName;

@end

@implementation ModuleFourRouter

- (instancetype)init
{
    self = [super init];
    if (self) {
        _deeplinkName = [[FourRouterDeeplinkNames alloc] init];
    }
    return self;
}


// MARK: - Private

- (void)navigateView:(UIViewController *)view
        withDeeplink:(Deeplink *)deeplink
          andNavType:(DeeplinkNavigationType)navType {
    switch (navType) {
        case DeeplinkNavigationTypePush:
            [deeplink.sourceView.navigationController pushViewController:view animated:YES];
            break;
        default:
            break;
    }
}


#pragma mark - DeeplinksRouter

- (BOOL)canOpen:(Deeplink * _Nonnull)deeplink {
    NSArray<NSString *> *names = @[[FourRouterDeeplinkNames nameFourOne]];
    
    BOOL result = NO;
    NSURL *url = deeplink.sourceData;
    NSString *host = url.host;
    if([host isEqualToString:@"four"]) {
        NSString *firstPathComp = url.pathComponents[1];
        result = [names containsObject:firstPathComp];
    }
    return result;
}

- (void)open:(Deeplink * _Nonnull)deeplink {
    if([self canOpen:deeplink]) {
        NSString *name = deeplink.sourceData.pathComponents[1];
        if([name isEqualToString:[FourRouterDeeplinkNames nameFourOne]]) {
            FourOneViewController *vc = [[FourOneViewController alloc] init];
            [self navigateView:vc withDeeplink:deeplink andNavType:DeeplinkNavigationTypePush];
        }
    }
}

@end
