//
//  FourRouterDeeplinkNames.h
//  ModuleFour
//
//  Created by Гутовец Дмитрий on 06.05.2021.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FourRouterDeeplinkNames : NSObject

+ (NSString *)nameFourOne;

@end

NS_ASSUME_NONNULL_END
