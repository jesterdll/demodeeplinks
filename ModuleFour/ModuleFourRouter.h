//
//  ModuleFourRouter.h
//  ModuleFour
//
//  Created by Гутовец Дмитрий on 05.05.2021.
//

#import <Foundation/Foundation.h>
#import <ModuleDeeplinks/ModuleDeeplinks.h>

NS_ASSUME_NONNULL_BEGIN

@interface ModuleFourRouter : NSObject <DeeplinksRouter>

@end

NS_ASSUME_NONNULL_END
