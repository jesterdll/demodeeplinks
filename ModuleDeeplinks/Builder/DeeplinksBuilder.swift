//
//  DeeplinksBuilder.swift
//  ModuleDeeplinks
//
//  Created by Гутовец Дмитрий on 29.04.2021.
//

import Foundation
import UIKit


protocol DeeplinksBuilder {
    var output: Deeplink { get }
    func setInput(deeplink: Deeplink)
    func addSource(view: UIViewController?)
}

final class DeeplinksBuilderImp: DeeplinksBuilder {
    
    private(set) var deeplink: Deeplink!
    var output: Deeplink {
        return deeplink
    }
    
    init() {
        print("-- \(type(of: self)) inited")
    }
    
    // Пока не понятно нужно ли это
    // Задаем диплинк, который нужно достроить
    func setInput(deeplink: Deeplink) {
        self.deeplink = deeplink
    }
    
    // Задаем source экран
    func addSource(view: UIViewController?) {
        deeplink.sourceView = view
    }
}
