//
//  DeeplinkFactory.swift
//  ModuleDeeplinks
//
//  Created by Гутовец Дмитрий on 29.04.2021.
//

import Foundation

protocol DeeplinkFactory {
    func createFrom(url: URL, sourceType: DeeplinkSourceType) -> Deeplink?
}

final class DeeplinkFactoryImp: DeeplinkFactory {
    
    let prefix = "deeplinks://" // Будет cian://
    
    init() {
        print("-- \(type(of: self)) inited")
    }
    
    // Фиктивный метод
    func createFrom(url: URL, sourceType: DeeplinkSourceType) -> Deeplink? {
        // Фиктивная реализация
        let rawUrl = url
        // Тут можно что-то сделать с исходником, если нужно
        
        return Deeplink(source: rawUrl, sourceType: sourceType)
    }
}


extension String {
    func deletePrefix(_ prefix: String) -> String {
        guard self.hasPrefix(prefix) else {
            return self
        }
        return String(self.dropFirst(prefix.count))
    }
}
