//
//  Deeplink.swift
//  ModuleDeeplinks
//
//  Created by Гутовец Дмитрий on 29.04.2021.
//

import Foundation
import  UIKit


@objc public class Deeplink: NSObject {
    @objc public var sourceData: URL
    @objc public var sourceView: UIViewController?
    @objc public var sourceType: DeeplinkSourceType = .unknown
    
    @objc init(source: URL,
               sourceView: UIViewController? = nil,
               sourceType: DeeplinkSourceType = .unknown) {
        self.sourceData = source
        self.sourceView = sourceView
        self.sourceType = sourceType
        
        print("--- \(type(of: self)) inited [source: \(source)]")
    }
}
