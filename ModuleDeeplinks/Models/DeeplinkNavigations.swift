//
//  DeeplinkNavigations.swift
//  ModuleDeeplinks
//
//  Created by Гутовец Дмитрий on 29.04.2021.
//

import Foundation

@objc public enum DeeplinkNavigationType: Int {
    case push
    case present
//    case window // пока скрою, т.к. нет самого механизма в демо-проекте
    case presentAfterDismiss
}

// TODO: Вопрос: Нужно ли передавать тип перехода или это в роутере конкретном уже решаться будет?
