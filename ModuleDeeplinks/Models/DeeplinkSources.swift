//
//  DeeplinkSources.swift
//  ModuleDeeplinks
//
//  Created by Гутовец Дмитрий on 02.05.2021.
//

import Foundation

// Случай, когда мы используем простой (не конкретизируем внутренний переход)

@objc public enum DeeplinkSourceType: Int {
    case unknown
    case push
    case deeplink
    case shortcut
    case universal
    case deferred
    case internalLink
}


// Если мы хотим сделать, чтобы внутренние переходы можно было
// точнее идентифицировать и параметризировать, нужно делать source как протокол/класс

// Тогда в каждом модуле можно сделать Фабрику (или даже класс с статическими свойствами)
// В котором был бы список доступных внутренних переходов.

//// #1
//@objc protocol DeeplinkSourceType {
//    @objc var type: String { get }
//}
//
//// #2
//@objc class DeeplinkSourceType {
//
//    let type: String
//
//    init(type: String) {
//        self.type = type
//    }
//}
