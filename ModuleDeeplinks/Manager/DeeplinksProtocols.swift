//
//  DeeplinksProtocols.swift
//  ModuleDeeplinks
//
//  Created by Гутовец Дмитрий on 29.04.2021.
//

import Foundation
import UIKit


// Базовый протокол для основных действий, выделяется, потому что
// переиспользуется в менеджере, роутере и хранилище роутеров
@objc public protocol DeeplinksGeneralActions {
    @objc func canOpen(_ deeplink: Deeplink) -> Bool
    @objc func open(_ deeplink: Deeplink)
}



public typealias DeeplinksManager = DeeplinksManagerActionable & DeeplinksManagerRegisterable

// Протокол для возможных действий
@objc public protocol DeeplinksManagerActionable: DeeplinksGeneralActions {
    // Методы для строк
    func canOpen(raw deeplinkUrl: URL, source: DeeplinkSourceType) -> Bool
    func open(raw deeplinkUrl: URL, source: DeeplinkSourceType)
}

// Протокол для функционала регистрации роутеров в модуле
@objc public protocol DeeplinksManagerRegisterable: DeeplinksRouterRegisterable {
    
}



@objc public protocol DeeplinksManagerDelegate: AnyObject {
    // Нужно для получения верхнего контроллера, от которого будет открываться
    func getSourceView() -> UIViewController
}
