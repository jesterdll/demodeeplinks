//
//  DeeplinksManager.swift
//  ModuleDeeplinks
//
//  Created by Гутовец Дмитрий on 29.04.2021.
//

import Foundation

// Реализация менеджера диплинков
// реализует паттерн Фасад, много прокси методов
@objc public class DeeplinksMainManager: NSObject, DeeplinksManager {
    private let routesrStorage: DeeplinksRoutesrStorage
    private lazy var deeplinkFactory: DeeplinkFactory = {
        return DeeplinkFactoryImp()
    }()
    private lazy var deeplinkBuilder: DeeplinksBuilder = {
        return DeeplinksBuilderImp()
    }()
    private weak var delegate: DeeplinksManagerDelegate?
    
    // Сделал пока для удобства тестов
    public static let global: DeeplinksManager = DeeplinksMainManager(delegate: nil)
    
    @objc public init(delegate: DeeplinksManagerDelegate?) {
        print("- \(type(of: self)) inited")
        self.delegate = delegate
        self.routesrStorage = DeeplinksRoutesrStorageImp()
    }
    
    
    // MARK: - Private
    
    private func buildDeeplink(from deeplinkUrl: URL, sourceType: DeeplinkSourceType) -> Deeplink? {
        guard let deeplink = deeplinkFactory.createFrom(url: deeplinkUrl, sourceType: sourceType) else {
            return nil
        }
        return buildAdditionalInfo(deeplink)
    }
    
    @discardableResult
    private func buildAdditionalInfo(_ deeplink: Deeplink) -> Deeplink? {
        deeplinkBuilder.setInput(deeplink: deeplink)
        deeplinkBuilder.addSource(view: delegate?.getSourceView())
        
        return deeplinkBuilder.output
    }
}


// MARK: - DeeplinksManagerRegisterable

extension DeeplinksMainManager {
    public func register(router: DeeplinksRouter) {
        routesrStorage.register(router: router)
    }
    
    public func register(routers list: [DeeplinksRouter]) {
        routesrStorage.register(routers: list)
    }
}


// MARK: - DeeplinksManagerActionable

extension DeeplinksMainManager {
    public func canOpen(_ deeplink: Deeplink) -> Bool {
        return routesrStorage.router(for: deeplink) != nil
    }
    
    public func open(_ deeplink: Deeplink) {
        guard let router = routesrStorage.router(for: deeplink) else {
            return
        }
        router.open(deeplink)
    }
    
    public func canOpen(raw deeplinkUrl: URL, source: DeeplinkSourceType) -> Bool {
        guard let deeplink = buildDeeplink(from: deeplinkUrl, sourceType: source) else {
            return false
        }
        return canOpen(deeplink)
    }
    
    public func open(raw deeplinkUrl: URL, source: DeeplinkSourceType) {
        guard let deeplink = buildDeeplink(from: deeplinkUrl, sourceType: source) else {
            return
        }
        open(deeplink)
    }
}
