//
//  DeeplinksRoutersProtocol.swift
//  ModuleDeeplinks
//
//  Created by Гутовец Дмитрий on 29.04.2021.
//

import Foundation


// Протокол для внешнего интерфейса роурета, примитивный -
// функционал ограничен требованиями: проверять и открывать
@objc public protocol DeeplinksRouter: DeeplinksGeneralActions {
    // Нет специфичных методов пока
}

// TODO: Тут пока не понятно, возможно придется создать базовый класс для наследования


@objc public protocol DeeplinksRouterRegisterable {
    func register(router: DeeplinksRouter)
    func register(routers list: [DeeplinksRouter])
}
