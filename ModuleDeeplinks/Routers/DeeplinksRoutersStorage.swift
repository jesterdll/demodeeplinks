//
//  DeeplinksRoutesrStorage.swift
//  ModuleDeeplinks
//
//  Created by Гутовец Дмитрий on 29.04.2021.
//

import Foundation


// Описан на основе DeeplinksRouterRegisterable - там регистрация
protocol DeeplinksRoutesrStorage: DeeplinksRouterRegisterable {
    func router(for deeplink: Deeplink) -> DeeplinksRouter?
}


final class DeeplinksRoutesrStorageImp: DeeplinksRoutesrStorage {
    private var routers: [DeeplinksRouter] = []
    
    init() {
        print("-- \(type(of: self)) inited")
    }
    
    
    // MARK: - DeeplinksRouter
    
    func register(router: DeeplinksRouter) {
        routers.append(router)
    }
    
    func register(routers list: [DeeplinksRouter]) {
        routers += list
    }
    
    func router(for deeplink: Deeplink) -> DeeplinksRouter? {
        var result: DeeplinksRouter? = nil
        for router in routers {
            if router.canOpen(deeplink) {
                result = router
                break
            }
        }
        return result
    }
}
