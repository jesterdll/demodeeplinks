//
//  OneOneViewController.swift
//  ModuleOne
//
//  Created by Гутовец Дмитрий on 29.04.2021.
//

import UIKit

class OneOneViewController: UIViewController {

    public init() {
        super.init(nibName: String(describing: type(of: self)), bundle: Bundle(for: type(of: self).self))
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

}
